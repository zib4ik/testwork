<?php

	$config = [
		'site-name' => 'Task Manager',
		'db' => [
			'hostname' => 'localhost',
			'username' => 'root',
			'password' => '',
			'dbname' => 'tasklist'
		]
	];

	return $config;