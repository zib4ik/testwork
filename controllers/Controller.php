<?php

namespace Controllers;

class Controller
{
	public $controller = null;
	public $action = null;
	public $title = null;
	public $page_name = null;

	public function __construct($config = [])
	{
		foreach($config as $key => $_config)
		{
			$this->$key = $_config;
		}
	}

	public function render($data = [])
	{
		$data['title'] = $this->title;
		$data['page_name'] = $this->page_name;
		extract($data);

		ob_start();
		include( APP_PATH . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'header.php' );
		include( APP_PATH . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $this->controller . DIRECTORY_SEPARATOR . $this->action .'.php' );
		include( APP_PATH . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'footer.php' );
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}
}