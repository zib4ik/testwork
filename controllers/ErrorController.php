<?php

namespace Controllers;

use Controllers\Controller;

class ErrorController extends Controller
{
	public function actionError($error)
	{
		header($error);
		return $this->render();
	}
}