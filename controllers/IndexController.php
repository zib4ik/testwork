<?php

namespace Controllers;

use Controllers\Controller;
use Models\Tasks;

class IndexController extends Controller
{
	public function actionIndex()
	{
		if (!isset($_SESSION['sort_by'])) $_SESSION['sort_by'] = 'id';
		if (!isset($_SESSION['sort'])) $_SESSION['sort'] = 'desc';

		if ($_GET['sort_by']) $_SESSION['sort_by'] = $_GET['sort_by'];
		if ($_GET['sort']) $_SESSION['sort'] = $_GET['sort'];

		if (!isset($_GET['page'])) $_GET['page'] = 1;
		$_GET['page'] = intval($_GET['page']);

		$tasks = new Tasks();

		$limit = 3;
		$offset = $limit*($_GET['page']-1);
		$totalCount = $tasks->count();

		$pages = 0;
		if ($totalCount>$limit) $pages = ceil($totalCount/$limit);

		$order = $_SESSION['sort_by'].' '.strtoupper($_SESSION['sort']);
		$tasks->find(false, "{$order}", "LIMIT {$limit} OFFSET {$offset}");
		return $this->render([
			'item' => $tasks,
			'page' => $_GET['page'],
			'limit' => $limit,
			'pages' => $pages
		]);
	}
}