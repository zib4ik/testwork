<?php

namespace Controllers;

use Controllers\Controller;
use Models\Tasks;
use Models\User;

class TaskController extends Controller
{
	public function actionCreate()
	{
		$this->page_name = 'Создать задачу';
		$this->title = $this->page_name.' - '.$this->title;

		$task = new Tasks();
		if (!User::isAuth()) header('Location: /user/login ');

		if ($_POST)
		{
			$task->username = $_POST['username'];
			$task->email = $_POST['email'];
			$task->text = $_POST['text'];
			$task->save();

			if (!$task->errors)
			{
				header('Location: / ');
			}
		}

		return $this->render([
			'item' => $task
		]);
	}

	public function actionEdit()
	{
		$this->page_name = 'Редактирование задачи';
		$this->title = $this->page_name.' - '.$this->title;

		$id = isset($_GET['id']) ? intval($_GET['id']) : false;
		if (!$id) header('Location: / ');
		if (!User::isAuth()) header('Location: /user/login ');

		$task = new Tasks();
		$task->findOne($id);

		if ($_POST)
		{
			$task->username = $_POST['username'];
			$task->email = $_POST['email'];
			if ($_POST['text']!=$task->text) $task->isUpdated = 1;
			$task->text = $_POST['text'];
			if ($_POST['status'])
			{
				$task->status = 1;
			} else {
				$task->status = 0;
			}
			$task->save();

			if (!$task->errors)
			{
				header('Location: / ');
			}
		}

		return $this->render([
			'item' => $task
		]);
	}
}