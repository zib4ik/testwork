<?php

namespace Controllers;

use Controllers\Controller;
use Models\User;

class UserController extends Controller
{
	public function actionLogin()
	{
		$this->page_name = 'Авторизация';
		$this->title = $this->page_name.' - '.$this->title;

		$user = false;
		if (User::isAuth()) header('Location: / ');

		if ($_POST)
		{
			$user = User::login($_POST['login'], $_POST['password']);

			if ($user['result']==true)
			{
				header('Location: / ');
			}
		}

		return $this->render([
			'user' => $user
		]);
	}

	public function actionLogout()
	{
		if (User::logout()) header('Location: / ');
	}
}