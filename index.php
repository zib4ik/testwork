<?php

	define('APP_PATH', __DIR__);
	require_once APP_PATH . '/vendor/autoload.php';
	$config = require APP_PATH . '/config/web.php';

	use Models\System;

	$app = new System();
	$app->runApp($config);