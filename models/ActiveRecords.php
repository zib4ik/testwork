<?php

namespace Models;

use Models\Validator;

class ActiveRecords
{
	private $connection;
	public $table;
	public $schema;
	public $result;
	public $errors = null;
	public $validate = [];

	function __construct()
	{
		$config = require APP_PATH . '/config/web.php';
		$db_config = $config['db'];

		$this->connection = mysqli_connect($db_config['hostname'], $db_config['username'], $db_config['password'], $db_config['dbname']);
		mysqli_query($this->connection, "SET CHARACTER SET utf8");

		$this->getSchema();
	}

	public function getSchema()
	{
		$result = $this->query("SHOW COLUMNS FROM {$this->table}");
		if ($result)
		{
			while ($obj = mysqli_fetch_object($result))
			{
				$field = $obj->Field;
				$this->schema[$field] = $obj->Type;
				$this->$field = null;
			}
		}
	}

	public function find($where = false, $order = false, $limit = false)
	{
		$query = "SELECT * FROM `{$this->table}`";
		if ($where) $query .= " WHERE {$where}";
		if ($order) $query .= " ORDER By {$order}";
		if ($limit) $query .= " {$limit}";

		$result = $this->query($query);
		
		$items = [];
		
		if ($result)
		{
			while ($obj = mysqli_fetch_object($result))
			{
				$items[] = $obj;
			}
		}

		$this->result = $items;

		return $items;
	}

	public function count()
	{
		$result = mysqli_query($this->connection, "SELECT COUNT(`id`) as `count` FROM `{$this->table}`");
		$row = mysqli_fetch_object($result);
		$count = (int) $row->count;
		return $count;
	}

	public function findOne($id)
	{
		$items = $this->find("id = {$id}");
		$this->result = count($items) ? $items[0] : false;

		foreach($this->result as $key=>$item) {
			$this->$key = $item;
		}
	}

	public function query($sql)
	{
		return mysqli_query($this->connection, $sql);
	}

	public function save()
	{
		if( isset( $this->id ) && $this->id > 0 )
		{
			$this->update();
		}
		else{
			$this->insert();
		}
	}

	private function update()
	{
		$_sql = '';
		foreach($this->schema as $key => $value)
		{
			if ($key==='id') continue;

			$_sql .= "`" . $key . "` = ";
			$_sql .= "'" . htmlspecialchars($this->$key) . "', ";

			if ($this->validate[$key])
			{
				foreach($this->validate[$key] as $type)
				{
					if ($error = Validator::check(htmlspecialchars($this->$key), $type, $this->labels[$key])) $this->errors[$key] = $error;
				}
			}
		}

		if (empty($this->errors))
		{
			$_sql = substr($_sql,0,-2);
			$this->query("UPDATE `{$this->table}` SET {$_sql} WHERE `id`={$this->id}");
		}
	}

	private function insert()
	{
		$_sql_01 = $_sql_02 = "";
		foreach($this->schema as $key => $value)
		{
			if ($key==='id') continue;

			$_sql_01 .= "`" . $key . "`, ";
			$_sql_02 .= "'" . htmlspecialchars($this->$key) . "', ";

			if ($this->validate[$key])
			{
				foreach($this->validate[$key] as $type)
				{
					if ($error = Validator::check(htmlspecialchars($this->$key), $type, $this->labels[$key])) $this->errors[$key] = $error;
				}
			}
		}

		if (empty($this->errors))
		{
			$_sql_01 = substr( $_sql_01, 0, -2 );
			$_sql_02 = substr( $_sql_02, 0, -2 );

			$this->query( "INSERT INTO `{$this->table}` ({$_sql_01})" . " VALUES ({$_sql_02})" );

			$new_id   = mysqli_insert_id( $this->connection );
			$this->id = $new_id;
		}
	}

	public function delete()
	{
		$result = $this->query("DELETE FROM `{$this->table}` WHERE `id`=" . $this->id);
		return $result;
	}
}