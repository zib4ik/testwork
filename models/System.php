<?php

namespace Models;

use Controllers\ErrorController;
use Models\DbHelper;

class System
{
	public function runApp($config)
	{
		session_start();
		$data = $this->router($config);
		echo $data;
	}

	public function router($config) {
		$url = $_GET['url'];
		$url = rtrim( $url, '/' );
		$url = explode('/', $url );

		$url[0] = !empty($url[0]) ? strtolower($url[0]) : 'index';
		$url[1] = !empty($url[1]) ? strtolower($url[1]) : 'index';

		$className = ucfirst($url[0]) . 'Controller';
		$actionName = 'action' . ucfirst($url[1]);
		$controllerFile =  APP_PATH . DIRECTORY_SEPARATOR.  'controllers' . DIRECTORY_SEPARATOR . $className . '.php';

		if (file_exists($controllerFile))
		{
			require $controllerFile;

			$fullClassName = 'Controllers\\'.$className;
			if (class_exists($fullClassName))
			{
				$controller = new $fullClassName([
					'title' => $config['site-name'],
					'controller' => $url[0],
					'action' => $url[1]
				]);
				if (method_exists($controller, $actionName))
				{
					$result = $controller->$actionName();
					return $result;
				}
			}
		}

		$controller = new ErrorController([
			'title' => 'Ошибка 404 - '.$config['site-name'],
			'page_name' => 'Ошибка 404',
			'controller' => 'index',
			'action' => 'error'
		]);
		$result = $controller->actionError("HTTP/1.0 404 Not Found");

		return $result;
	}
}