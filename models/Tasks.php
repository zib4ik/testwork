<?php

namespace Models;

use Models\ActiveRecords;

class Tasks extends ActiveRecords
{
	public $table = 'tasks';
	public $validate = [
		'username' => ['valid'],
		'email' => ['valid','email'],
		'text' => ['valid']
	];
	public $labels = [
		'username' => 'Имя пользователя',
		'email' => 'E-mail',
		'text' => 'Текст задачи',
		'status' => 'Статус',
	];
	public $statuses = [
		0 => 'Создана',
		1 => 'Выполнена'
	];
}