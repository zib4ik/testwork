<?php

namespace Models;

class User
{
	private $solt = '876bd&^ADuh';
	public $user = [
		'id' => 1,
		'login' => 'admin',
		'password' => '123'
	];
	public $errors = [];

	public static function isAuth()
	{
		$user = new self();
		$solt = md5($user->solt.json_encode($user->user));

		if ($_SESSION['user'] && $solt==$_SESSION['user'])
		{
			return true;
		}

		return false;
	}

	public static function login($login, $password)
	{
		$user = new self();

		if ($login == '') $user->errors['login'] = 'Логин не может быть пустым.';
		if ($password == '') $user->errors['password'] = 'Пароль не может быть пустым.';
		if ($login != $user->user['login'] || $password != $user->user['password']) $user->errors['all'] = 'Логин или пароль введены не верно.';

		if (empty($user->errors))
		{
			$solt = md5($user->solt.json_encode($user->user));
			$_SESSION['user'] = $solt;

			return ['result' => true];
		}

		return [
			'result' => false,
			'errors' => $user->errors
		];
	}

	public static function logout()
	{
		unset($_SESSION['user']);
		return true;
	}
}