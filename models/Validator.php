<?php

namespace Models;

class Validator
{
	public static function check($value, $type, $label)
	{
		switch ($type) {
			case 'valid':
					if ($value=='') return "{$label}, не может быть пустым.";
				break;
			case 'email':
					if (!filter_var($value, FILTER_VALIDATE_EMAIL)) return "{$label}, поле заполнено не корректно.";
				break;
		}

		return false;
	}
}