<div class="container">
	<div class="card">
		<div class="card-header">
			<?php echo $page_name; ?>
		</div>
		<div class="card-body">
			<p class="card-text">Что то пошло не так!</p>
			<a href="/" class="btn btn-primary">Вернутся на главную</a>
		</div>
	</div>
</div>