<?php
    use Models\User;
?>
<div class="container">
    <!--Table-->
    <table id="tablePreview" class="table table-striped table-bordered table-sm dataTable">
        <!--Table head-->
        <thead>
            <tr>
                <th>
                    <?php if ($_SESSION['sort_by']=='id' && $_SESSION['sort']=='asc'): ?>
                        <a class="d-block" href="/?sort_by=id&sort=desc"># <i class="fas fa-sort-up float-right"></i></a>
                    <?php elseif ($_SESSION['sort_by']=='id' && $_SESSION['sort']=='desc'): ?>
                        <a class="d-block" href="/?sort_by=id&sort=asc"># <i class="fas fa-sort-down float-right"></i></a>
                    <?php else: ?>
                        <a class="d-block" href="/?sort_by=id&sort=asc"># <i class="fas fa-sort float-right"></i></a>
                    <?php endif; ?>
                </th>
                <th>
					<?php if ($_SESSION['sort_by']=='username' && $_SESSION['sort']=='asc'): ?>
                        <a class="d-block" href="/?sort_by=username&sort=desc">Имя пользователя <i class="fas fa-sort-up float-right"></i></a>
					<?php elseif ($_SESSION['sort_by']=='username' && $_SESSION['sort']=='desc'): ?>
                        <a class="d-block" href="/?sort_by=username&sort=asc">Имя пользователя <i class="fas fa-sort-down float-right"></i></a>
					<?php else: ?>
                        <a class="d-block" href="/?sort_by=username&sort=asc">Имя пользователя <i class="fas fa-sort float-right"></i></a>
					<?php endif; ?>
                </th>
                <th>
					<?php if ($_SESSION['sort_by']=='email' && $_SESSION['sort']=='asc'): ?>
                        <a class="d-block" href="/?sort_by=email&sort=desc">E-mail <i class="fas fa-sort-up float-right"></i></a>
					<?php elseif ($_SESSION['sort_by']=='email' && $_SESSION['sort']=='desc'): ?>
                        <a class="d-block" href="/?sort_by=email&sort=asc">E-mail <i class="fas fa-sort-down float-right"></i></a>
					<?php else: ?>
                        <a class="d-block" href="/?sort_by=email&sort=asc">E-mail <i class="fas fa-sort float-right"></i></a>
					<?php endif; ?>
                </th>
                <th>
					<?php if ($_SESSION['sort_by']=='text' && $_SESSION['sort']=='asc'): ?>
                        <a class="d-block" href="/?sort_by=text&sort=desc">Текст задачи <i class="fas fa-sort-up float-right"></i></a>
					<?php elseif ($_SESSION['sort_by']=='text' && $_SESSION['sort']=='desc'): ?>
                        <a class="d-block" href="/?sort_by=text&sort=asc">Текст задачи <i class="fas fa-sort-down float-right"></i></a>
					<?php else: ?>
                        <a class="d-block" href="/?sort_by=text&sort=asc">Текст задачи <i class="fas fa-sort float-right"></i></a>
					<?php endif; ?>
                </th>
                <th>
					<?php if ($_SESSION['sort_by']=='status' && $_SESSION['sort']=='asc'): ?>
                        <a class="d-block" href="/?sort_by=status&sort=desc">Статус <i class="fas fa-sort-up float-right"></i></a>
					<?php elseif ($_SESSION['sort_by']=='status' && $_SESSION['sort']=='desc'): ?>
                        <a class="d-block" href="/?sort_by=status&sort=asc">Статус <i class="fas fa-sort-down float-right"></i></a>
					<?php else: ?>
                        <a class="d-block" href="/?sort_by=status&sort=asc">Статус <i class="fas fa-sort float-right"></i></a>
					<?php endif; ?>
                </th>
				<?php if (User::isAuth()): ?>
                    <th></th>
                <?php endif; ?>
            </tr>
        </thead>
        <!--Table head-->
        <!--Table body-->
        <tbody>
            <?php foreach($item->result as $_item): ?>
                <tr>
                    <th scope="row"><?php echo $_item->id; ?></th>
                    <td><?php echo $_item->username; ?></td>
                    <td><?php echo $_item->email; ?></td>
                    <td><?php echo $_item->text; ?></td>
                    <td>
                        <span class="badge badge-pill badge-info"><?php echo $item->statuses[$_item->status]; ?></span>
                        <?php if ($_item->isUpdated): ?>
                            <br /><span class="badge badge-pill badge-success">Отредактировано администратором</span>
                        <?php endif; ?>
                    </td>
					<?php if (User::isAuth()): ?>
                        <td><a class="btn btn-success btn-sm d-block" href="/task/edit?id=<?php echo $_item->id; ?>"><i class="fas fa-edit"></i></a></td>
					<?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <!--Table body-->
    </table>
    <!--Table-->
    <?php if ($pages>0): ?>
        <nav aria-label="Page navigation example">
            <ul class="pagination pg-blue justify-content-center">
                <?php for ($i=1; $pages>=$i; $i++): ?>
                    <?php if ($i==1): ?>
                        <li class="page-item<?php echo $_GET['page']==$i ? ' active' : ''; ?>"><a href="/" class="page-link"><?php echo $i; ?></a></li>
                    <?php else: ?>
                        <li class="page-item<?php echo $_GET['page']==$i ? ' active' : ''; ?>"><a href="/?page=<?php echo $i; ?>" class="page-link"><?php echo $i; ?></a></li>
                    <?php endif; ?>
                <?php endfor; ?>
            </ul>
        </nav>
    <?php endif; ?>
</div>