<?php
	use Models\User;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo $title; ?></title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">

	<!-- Material Design for Bootstrap -->
	<link rel="stylesheet" href="/assets/css/mdb.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">

    <link rel="stylesheet" href="/assets/css/style.css">
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark indigo">
	<div class="container">
		<a class="navbar-brand mdb-logo" href="/">TestWork</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarExample"
				aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarExample">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="/">Главная</a>
				</li>
			</ul>
			<ul class="navbar-nav">
				<?php if (User::isAuth()): ?>
					<li class="nav-item">
						<a class="nav-link waves-effect waves-light" href="/task/create"><i class="fas fa-plus"></i> <span class="clearfix d-none d-sm-inline-block">Добавить задачу</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link waves-effect waves-light" href="/user/logout"><i class="fas fa-user"></i> <span class="clearfix d-none d-sm-inline-block">Выйти</span></a>
					</li>
				<?php else: ?>
					<li class="nav-item">
						<a class="nav-link waves-effect waves-light" href="/user/login"><i class="fas fa-user"></i> <span class="clearfix d-none d-sm-inline-block">Войти</span></a>
					</li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</nav>
<!-- /.Navbar -->
<br />
<br />