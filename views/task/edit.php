<div class="container">
	<?php if ($item->errors): ?>
		<?php foreach ($item->errors as $error): ?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert" role="alert">
				<?php echo $error; ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
	<form method="POST" action="" class="border border-light p-5">
		<p class="h4 mb-4 text-center">Редактирование задачи</p>

		<input name="username" type="text" id="defaultContactFormName" class="form-control mb-4" value="<?php echo $item->username; ?>" placeholder="Имя пользователя" />

		<input name="email" type="email" id="defaultContactFormEmail" class="form-control mb-4" value="<?php echo $item->email; ?>" placeholder="E-mail" />

		<textarea name="text" class="form-control rounded-0 mb-4" id="exampleFormControlTextarea2" rows="3" placeholder="Текст задачи"><?php echo $item->text; ?></textarea>

		<div class="custom-control custom-checkbox mb-4">
			<input name="status"<?php echo $item->status==1 ? ' checked="checked"' : ''; ?> type="checkbox" class="custom-control-input" id="defaultContactFormCopy">
			<label class="custom-control-label" for="defaultContactFormCopy">Выполнена</label>
		</div>

		<button class="btn btn-info btn-block" type="submit">Создать</button>
	</form>
</div>