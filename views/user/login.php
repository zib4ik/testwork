<div class="container">
	<?php if ($user['errors']): ?>
		<?php foreach ($user['errors'] as $error): ?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert" role="alert">
				<?php echo $error; ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
	<form action="" method="POST" class="border border-light p-5">
		<p class="h4 mb-4 text-center">Авторизация</p>
		<input name="login" type="text" id="defaultLoginFormEmail" class="form-control mb-4" value="<?php echo $_POST['login']; ?>" placeholder="Логин">
		<input name="password" type="password" id="defaultLoginFormPassword" class="form-control mb-4"  value="<?php echo $_POST['password']; ?>" placeholder="Пароль">
		<button class="btn btn-info btn-block my-4" type="submit">Войти</button>
	</form>
</div>